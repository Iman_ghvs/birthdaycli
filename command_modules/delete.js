//This module exports a function to delete a selected entry
//and another to clear the database

const readline = require('readline-sync');
const chalk = require('chalk');

const {db} = require('../db');
const {confirm} = require('../utils');

exports.delEntry = (enteredName) => {
    // if no name has entered, we should ask it!
    name = enteredName || readline.question('Please enter a name ');
    
    db
        .get(name)
        .then(() => {
            confirm({
                warning: `This will delete the ${name}'s birthday entry!`,
                question: 'Are you sure to delete it? (y/n) '
            });
            db.del(name);
            console.log(chalk.green('Done!'));
        }).catch((err) => {
            if (err.notFound) console.log(`${name}'s birthday is not stored`);
            else console.log(err.message);
        })
}

exports.delAll = () => {
    try {
        confirm({
            warning: `This will delete all the birthday entries!`,
            question: 'Are you sure to continue? (y/n) '
        });
        db.clear().then(() => console.log(chalk.green('Done!')));
    } catch(err) {
        console.log(err.message);
    }
}