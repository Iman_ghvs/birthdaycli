// This module exports a function to log all the birthdays
// and another to log only the remaining birthdays of the year

const readline = require('readline-sync');
const chalk = require('chalk');
const {db} = require('../db');
let {shamsiMonths, daySuffix} = require('../utils');

const print = (filter = (m, d) => true) =>  {
    db.createReadStream()
        .on('data', ({key, value}) => {
            const name = key.toString();
            const [month, day] = JSON.parse(value.toString());
            if (!filter || filter(month, day)) {
                const monthName = shamsiMonths[month - 1];
                console.log(chalk.green(`${name}: birthday on ${day}${daySuffix(day)} day of month ${monthName}`));
        
            }
        })
}

exports.printList = () => print();

exports.printRemainings = () => {
    console.log(
        chalk.blue("You have requested to see a list of the remaining birthdays of this year.")
        ,"please tell us what date is today, in Hejri Shamsi!");

    const thisMonth = readline.questionInt('Which month? (in numbers) ');
    const today = readline.questionInt('Which day? ');
    
    print((month, day) => 
        (month > thisMonth) || ((month == thisMonth) && (day >= today)) ? true : false);
        
}