// This module exports a function to get entries of someone's birthday as input
// It also exports an array of shamsi months

const readline = require('readline-sync');
const chalk = require('chalk');

const {db} = require('../db');
const {shamsiMonths, confirm, daySuffix} = require('../utils');




exports.addEntry = () => {
    const name = readline.question('Enter a name: ');

    // build a promise to search the db to see whether the name already exists
    db
        .get(name)
        .then(() => {
            confirm({warning: `You have already entered ${name}'s birthday!`,
                    question: 'Are you sure to overwrite? (y/n)'});
        })
    
        // unexpected error, or canceling the operation by the client
        .catch((err) => {
            if (!err.notFound) throw new Error(err.message);
        })

        // already exist or not, we want to get details and save them in the database.        
        .then(() => {
            const month = readline.questionInt('In which month is his/her birthday? (enter a number between 1 and 12) ');
    
            // the month should be valid
            if ((month < 1) || (month > 12)) {
                throw new Error('Invalid month! It should be betweeen 1 and 12');
            }
            
            // first 6 months of a year have 31 days, others have 30
            const maxDay = month <= 6 ? 31 : 30;

            const day = readline.questionInt(`In which day of the month is his/her birthday? (enter a number between 1 and ${maxDay}) `);
            if (day > maxDay || day < 0) throw new Error('Invalid day entered');
            
            db.put(name, JSON.stringify([month, day]), (err) => console.log(err));

            let monthName = shamsiMonths[month - 1];
            console.log(chalk.green(`The name ${name} is submitted with the birthday ${day}${daySuffix(day)} of month ${monthName}.`));
        
        }) .catch (err => console.log(err.message))
}