const levelup = require('levelup');
const leveldown = require('leveldown');

exports.db = levelup(leveldown('birthDayDB'));