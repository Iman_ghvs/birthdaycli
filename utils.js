const chalk = require('chalk');
const readline = require('readline-sync');

// utilities

// months of a shamsi year

exports.shamsiMonths = [
    'Farvardin', 'Ordibehesht', 'Khordad',
    'Tir', 'Mordad', 'Shahrivar',
    'Mehr', 'Aban', 'Azar',
    'Dey', 'Bahman', 'Esfand'
];

exports.confirm = ({warning, question}) => {
    console.log(chalk.red(warning));
    const sure = readline.question(question);
    if (sure.toLowerCase() !== 'y') throw new Error('You have canceled the operation.');
    return;
}

exports.daySuffix = day => 
    (day % 10 === 1) ? 'st':
    (day % 10 === 2) ? 'nd':
    (day % 10 === 3) ? 'rd': 'th'; 