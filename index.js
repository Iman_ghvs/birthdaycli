#!/usr/bin/env node
'use strict';
const {program} = require('commander');
const {addEntry} = require('./command_modules/addEntry.js');
const {printList, printRemainings} = require('./command_modules/print.js');
const {delEntry, delAll} = require('./command_modules/delete.js');

// birthday add -> add a new birthday to db
program
    .command('add')
    .description('Add a new birthday')
    .action(addEntry);

// birthday delete -> deletes an entry (someone's birthday)
// the client can enter the name after the delete keyword
program
    .command('delete [theName]')
    .option('-a, --all', 'deletes all the entries')
    .description("Deletes an entry (someone's birthday)")
    .action((theName, cmdObj) => {
        if (cmdObj.all) delAll();
        else delEntry(theName);
    });

// birthday list -> list all the birthdays
// with an option to list only the remaining birthdays (list -r)
program
    .command('list')
    .option('-r, --remaining', 'just show the remaining birthdays of this year')
    .description('Show the list of birthdays')
    .action((cmdObj)=> {
        if (cmdObj.remaining) printRemainings();
        else printList();
    });

program.parse(process.argv);

